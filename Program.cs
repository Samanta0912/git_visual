﻿using System;
/*Даны два числа. Вывести вначале большее, а затем меньшее из них
 */

namespace maxmin
{
    internal class Program
    {
        static void Main()
        {
            Console.WriteLine("Введите число a:");
            int a = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите число b:");
            int b = int.Parse(Console.ReadLine());
            if (a>b)
            {
                Console.WriteLine($"Большее число: {a} , меньшее число: {b}");
            }
            else
            {
                Console.WriteLine($"Большее число: {b} , меньшее число: {a}");
            }
            Console.ReadKey();





        }
    }
}
